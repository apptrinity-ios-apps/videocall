/*
 * Copyright @ 2019-present 8x8, Inc.
 * Copyright @ 2017-2018 Atlassian Pty Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import UIKit
//import JitsiMeet
import JitsiMeetSDK


@available(iOS 13.0, *)
class ViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var displayNameTFT: UITextField!
    @IBOutlet weak var meetingIDTFT: UITextField!
    
    
    
    @IBOutlet weak var videoButton: UIButton?

    fileprivate var pipViewCoordinator: PiPViewCoordinator?
    fileprivate var jitsiMeetView: JitsiMeetView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
         overrideUserInterfaceStyle = .light
        
        videoButton?.layer.cornerRadius = (videoButton?.frame.size.height)! / 2
        displayNameTFT.layer.borderColor = UIColor.lightGray.cgColor
        meetingIDTFT.layer.borderColor = UIColor.lightGray.cgColor
        
        displayNameTFT.layer.borderWidth = 1
        meetingIDTFT.layer.borderWidth = 1

        displayNameTFT.layer.cornerRadius = 5
        meetingIDTFT.layer.cornerRadius = 5
        
        
        displayNameTFT.delegate = self
        meetingIDTFT.delegate = self
        

        
    }

    override func viewWillTransition(to size: CGSize,
                                     with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        let rect = CGRect(origin: CGPoint.zero, size: size)
        pipViewCoordinator?.resetBounds(bounds: rect)
    }

    // MARK: - Actions

    func JoinMeeting(){
        cleanUp()
              // create and configure jitsimeet view
              let jitmeet = JitsiMeetUserInfo()
        jitmeet.displayName = displayNameTFT.text
              let jitsiMeetView = JitsiMeetView()
              jitsiMeetView.delegate = self
              self.jitsiMeetView = jitsiMeetView
              let options = JitsiMeetConferenceOptions.fromBuilder { (builder) in
                builder.welcomePageEnabled = false
                builder.serverURL = URL(string: "https://inforlinx.xyz")
                builder.room = self.meetingIDTFT.text
                builder.userInfo = jitmeet
                
                
                
                builder.setFeatureFlag("pip.enabled", withBoolean: false)
                builder.setFeatureFlag("add-people.enabled", withBoolean: false)
                builder.setFeatureFlag("invite.enabled", withBoolean: false)
                builder.setFeatureFlag("raise-hand.enabled", withBoolean: false)
                  builder.setFeatureFlag("video-share.enabled", withBoolean: true)
             //  builder.setFeatureFlag("ios.screensharing.enabled", withBoolean: true)
               //   builder.setFeatureFlag("ios.screensharing.enabled", withBoolean: true)


              }

              jitsiMeetView.join(options)
            
                
              // Enable jitsimeet view to be a view that can be displayed
              // on top of all the things, and let the coordinator to manage
              // the view state and interactions
              pipViewCoordinator = PiPViewCoordinator(withView: jitsiMeetView)
              pipViewCoordinator?.configureAsStickyView(withParentView: view)
              // animate in
              jitsiMeetView.alpha = 0
              pipViewCoordinator?.show()
    }
    
    
    
    @IBAction func openJitsiMeet(sender: Any?) {
        
        self.view.endEditing(true)
        if displayNameTFT.text != "" && meetingIDTFT.text != "" {
            JoinMeeting()
        }else{
            let alert = UIAlertController(title: "Alert", message: "Please Enter vaild Fields.", preferredStyle: UIAlertController.Style.alert)
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

            // show the alert
            self.present(alert, animated: true, completion: nil)
            
        }
       
    }

    fileprivate func cleanUp() {
        jitsiMeetView?.removeFromSuperview()
        jitsiMeetView = nil
        pipViewCoordinator = nil
    }
}

@available(iOS 13.0, *)
extension ViewController: JitsiMeetViewDelegate {
    func conferenceTerminated(_ data: [AnyHashable : Any]!) {
        DispatchQueue.main.async {
            self.pipViewCoordinator?.hide() { _ in
                self.cleanUp()
                self.displayNameTFT.text = ""
                self.meetingIDTFT.text = ""
            }
        }
    }

    func enterPicture(inPicture data: [AnyHashable : Any]!) {
        DispatchQueue.main.async {
            self.pipViewCoordinator?.enterPictureInPicture()
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        textField.resignFirstResponder()
        return true
        
    }

}
   
      
